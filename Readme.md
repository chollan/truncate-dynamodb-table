# Dynamo DB Truncation script

designed to be used to read x rows and then delete n times until all records are deleted.

variables to set in the script:

* `TableKey` the key of the table that contains the data
* `Limit` number to read and delete at a time
* `TableName` the name of the table that contains the data

This script uses batch delete. unless you change how the delete works, don't increase the `Limit` variable to above 25 or you'll receive an error.