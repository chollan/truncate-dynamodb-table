const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient({region: 'us-east-1'});

(async () => {
    // set some parameters
    const TableKey = 'key';
    const Limit = 25; // the max is 25 as batching can only handle 25 at a time.
    const TableName = 'test_table';
    const readParams = { TableName, Limit };

    // define our items variable
    let items;
    do {
        // define our scan
        const itemsToDelete = [];

        // perfom our initial scan
        console.log('reading', Limit, 'records');
        items = await docClient.scan(readParams).promise();
        console.log(items.Items.length, 'records returned');

        // loop over all of these items and create a delete request
        items.Items.forEach(item => itemsToDelete.push({DeleteRequest: {Key: {[TableKey]: item[TableKey]}}}));

        // set our exclusive start key from the last evaluated key
        readParams.ExclusiveStartKey = items.LastEvaluatedKey;

        // execute a delete statement
        if (itemsToDelete.length > 0){
            console.log('deleting', itemsToDelete.length, 'records');

            // attempt to delete all of the items that we requestsed
            const {UnprocessedItems} = await docClient.batchWrite({RequestItems: {[TableName]: itemsToDelete}}).promise();

            // if there are deletion errors, lets log them
            if(Object.values(UnprocessedItems).length > 0){
                console.log('unprocessed values:', UnprocessedItems)
            }
        }

        // do it all over again
    }while(typeof items.LastEvaluatedKey !== "undefined");
})();